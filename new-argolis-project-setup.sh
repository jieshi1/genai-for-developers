#!/usr/bin/env bash

# Copyright 2023 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# New project setup - Run as your argolis org admin account
# Set your argolis org and user details
PROJECT_ID=devprod-genai-$RANDOM-$RANDOM
ORGANIZATION_ID=
BILLING_ACCOUNT=
GCP_USER_ACCOUNT=user@ldap.altostrat.com

gcloud projects create $PROJECT_ID \
    --organization=$ORGANIZATION_ID

gcloud beta billing projects link ${PROJECT_ID} \
    --billing-account=$BILLING_ACCOUNT

gcloud config set project ${PROJECT_ID}

gcloud services enable \
    orgpolicy.googleapis.com \
    aiplatform.googleapis.com \
    cloudaicompanion.googleapis.com \
    cloudfunctions.googleapis.com \
    run.googleapis.com \
    cloudbuild.googleapis.com \
    artifactregistry.googleapis.com


# Grant your GCP account owner role on the new project
gcloud projects add-iam-policy-binding $PROJECT_ID \
    --member=user:$GCP_USER_ACCOUNT \
    --role=roles/owner

echo "\nWaiting for IAM changes to propagate before running next steps\n"
sleep 2m

gcloud resource-manager org-policies disable-enforce iam.disableServiceAccountKeyCreation \
    --project=$PROJECT_ID

# Allowed ingress settings (Cloud Functions)
cat > allowedIngressSettings.yaml << ENDOFFILE
name: projects/$PROJECT_ID/policies/cloudfunctions.allowedIngressSettings
spec:
 rules:
 - allowAll: true
ENDOFFILE

gcloud org-policies set-policy allowedIngressSettings.yaml --project=$PROJECT_ID


# Allow Cloud Run public access
cat > allowedPolicyMemberDomains.yaml << ENDOFFILE
name: projects/$PROJECT_ID/policies/iam.allowedPolicyMemberDomains
spec:
 rules:
 - allowAll: true
ENDOFFILE

gcloud org-policies set-policy allowedPolicyMemberDomains.yaml --project=$PROJECT_ID

