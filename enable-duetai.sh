#!/usr/bin/env bash

# Copyright 2023 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

export PROJECT_ID=$(gcloud config get-value project)
export PROJECT_NUMBER=$(gcloud projects describe $PROJECT_ID --format='value(projectNumber)')
export PROJECT_NAME=$(gcloud projects describe $PROJECT_ID --format='value(name)')
export GCP_USER_ACCOUNT=$(gcloud config get-value account)

gcloud services enable \
    cloudaicompanion.googleapis.com

gcloud projects add-iam-policy-binding $PROJECT_ID \
    --member=user:$GCP_USER_ACCOUNT \
    --role=roles/cloudaicompanion.user

gcloud projects add-iam-policy-binding $PROJECT_ID \
    --member=user:$GCP_USER_ACCOUNT \
    --role=roles/serviceusage.serviceUsageViewer

