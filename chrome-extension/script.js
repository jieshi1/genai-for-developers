document.getElementById("codeForm").addEventListener("submit", function (event) {
    event.preventDefault();
    const userInput = document.getElementById("userInput").value;
    fetchData(userInput);
});

async function fetchData(code) {
    
    const res = await fetch("https://REGION-PROJECT_ID.cloudfunctions.net/devai", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ code: code })
    });
    const record = await res.json();
    document.getElementById("response").innerHTML = record.response[0].details;
}
